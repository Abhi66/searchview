package com.abhi.searchview.database

import android.arch.persistence.room.OnConflictStrategy
import android.content.ContentValues
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.RoomDatabase
import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Room
import android.content.Context
import android.database.Cursor


/**
 * @author Abhishek Prajapati
 * @version 1.0.0
 * @since 1/11/18.
 */
class DealRepository {
    fun getDealsDAO(context: Context): DealDao {
        return getDealsDatabase(context).dealDAO()
    }

    fun getDealsListInfo(context: Context, query: String): LiveData<List<DealInfo>> {
        return getDealsDAO(context).getDealsList(query)
    }

    fun getDealsCursor(context: Context, query: String): Cursor {
        return getDealsDAO(context).getDealsCursor(query)
    }

    companion object {
        private var dealsDatabase: DealsDatabase? = null
        private val LOCK = Any()

        @Synchronized
        fun getDealsDatabase(context: Context): DealsDatabase {
            if (dealsDatabase == null) {
                synchronized(LOCK) {
                    if (dealsDatabase == null) {
                        dealsDatabase = Room.databaseBuilder(context,
                                DealsDatabase::class.java, "DEALS DB")
                                .fallbackToDestructiveMigration()
                                .addCallback(dbCallback).build()

                    }
                }
            }
            return dealsDatabase!!
        }

        private val dbCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {

            }

            override fun onOpen(db: SupportSQLiteDatabase) {
                //first delete existing data and insert laates deals
                db.execSQL("Delete From DealInfo")

                var contentValues = ContentValues()
                contentValues.put("store", "Amazon")
                contentValues.put("deal", "60% off on fashion")
                contentValues.put("cashback", "3%")

                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "Amazon")
                contentValues.put("deal", "70% off on all mobiles")
                contentValues.put("cashback", "1%")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "JcPenney")
                contentValues.put("deal", "20% off on fashion")
                contentValues.put("cashback", "5%")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "JcPenney")
                contentValues.put("deal", "40% off on electronics")
                contentValues.put("cashback", "6%")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "Sears")
                contentValues.put("deal", "50% off on fashion")
                contentValues.put("cashback", "6%")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "Macys")
                contentValues.put("deal", "60% off on fashion")
                contentValues.put("cashback", "")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "Kohls")
                contentValues.put("deal", "36% off on fashion")
                contentValues.put("cashback", "")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "Walmart")
                contentValues.put("deal", "75% off on fashion")
                contentValues.put("cashback", "")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

                contentValues = ContentValues()
                contentValues.put("store", "Nordstrom")
                contentValues.put("deal", "")
                contentValues.put("cashback", "30% off on fashion")
                db.insert("DealInfo", OnConflictStrategy.IGNORE, contentValues)

            }
        }
    }
}