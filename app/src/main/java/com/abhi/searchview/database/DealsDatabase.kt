package com.abhi.searchview.database

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database



/**
 * @author Abhishek Prajapati
 * @version 1.0.0
 * @since 1/11/18.
 */
@Database(entities = [(DealInfo::class)], version = 1)
abstract class DealsDatabase : RoomDatabase() {
    abstract fun dealDAO(): DealDao
}