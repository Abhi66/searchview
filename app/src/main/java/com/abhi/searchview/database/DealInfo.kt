package com.abhi.searchview.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey



/**
 * @author Abhishek Prajapati
 * @version 1.0.0
 * @since 1/11/18.
 */
@Entity
class DealInfo {
    @PrimaryKey(autoGenerate = true)
    var _id: Int = 0
    var store: String? = null
    var deal: String? = null
    var cashback: String? = null
}