package com.abhi.searchview.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.database.Cursor

/**
 * @author Abhishek Prajapati
 * @version 1.0.0
 * @since 1/11/18.
 */

@Dao
interface DealDao {
    @Query("SELECT * FROM DealInfo WHERE deal LIKE :dealText")
    fun getDealsList(dealText: String): LiveData<List<DealInfo>>

    @Query("SELECT * FROM DealInfo WHERE deal LIKE :dealText")
    fun getDealsCursor(dealText: String): Cursor
}