package com.abhi.searchview

import android.content.Context
import android.widget.TextView
import com.abhi.searchview.database.DealInfo
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.ArrayAdapter


/**
 * @author Abhishek Prajapati
 * @version 1.0.0
 * @since 1/11/18.
 */
class SearchAdapter(private val mContext: Context, private val searchResultItemLayout: Int,
                    private val dataList: List<DealInfo>) : ArrayAdapter<DealInfo>(mContext, searchResultItemLayout, dataList) {

    override fun getCount(): Int {
        return dataList.size
    }

    override fun getItem(position: Int): DealInfo? {
        return dataList[position]
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        var view = view

        if (view == null) {
            view = LayoutInflater.from(parent.context)
                    .inflate(searchResultItemLayout, parent, false)
        }

        val di = getItem(position)

        val dealsTv = view!!.findViewById(R.id.tv_deal) as TextView
        dealsTv.text = di!!.deal

        val cashbackTv = view.findViewById(R.id.tv_cashback) as TextView
        cashbackTv.text = "Cash back " + di.cashback!!

//        val storeIb = view.findViewById(R.id.ib_store) as ImageView
//
//        val storeImageId = mContext.resources.getIdentifier(di.store!!.toLowerCase(),
//                "drawable", mContext.packageName)
//        storeIb.setImageResource(storeImageId)
        return view
    }
}